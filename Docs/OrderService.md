# Order Service API


<a name="overview"></a>
## Overview

### Version information
*Version* : v1




<a name="paths"></a>
## Paths

<a name="apiorderpositionspost"></a>
### Creates  an order position
```
POST /api/OrderPositions
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Body**|**orderPosition**  <br>*optional*|[OrderPosition](#orderposition)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* OrderPositions


<a name="apiorderpositionsget"></a>
### Gets all the Order positions
```
GET /api/OrderPositions
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [OrderPosition](#orderposition) > array|


#### Produces

* `application/json`


#### Tags

* OrderPositions


<a name="apiorderpositionsorderbyidget"></a>
### Gets all the order positions of an order
```
GET /api/OrderPositions/Order/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the parent order|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* OrderPositions


<a name="apiorderpositionsbyidget"></a>
### Gets an individual order position
```
GET /api/OrderPositions/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the order position|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* OrderPositions


<a name="apiorderpositionsbyiddelete"></a>
### Deletes an order position
```
DELETE /api/OrderPositions/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the order position to be deleted|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* OrderPositions


<a name="apiorderspost"></a>
### Adds a new order
```
POST /api/Orders
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Body**|**order**  <br>*optional*|[Order](#order)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Orders


<a name="apiordersget"></a>
### Gets all the existing orders
```
GET /api/Orders
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [Order](#order) > array|


#### Produces

* `application/json`


#### Tags

* Orders


<a name="apiordersbyidget"></a>
### Gets individual orders
```
GET /api/Orders/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the order|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* Orders


<a name="apiordersbyidput"></a>
### Updates the fields of an order
```
PUT /api/Orders/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the order to be updated|integer (int32)|
|**Body**|**order**  <br>*optional*||[Order](#order)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Orders


<a name="apiordersbyiddelete"></a>
### Deletes an order
```
DELETE /api/Orders/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the order to be deleted|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* Orders


<a name="apiproductspost"></a>
### Not implemented yet
```
POST /api/Products
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Body**|**value**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Products


<a name="apiproductsget"></a>
### Gets all the products
```
GET /api/Products
```


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [Product](#product) > array|


#### Produces

* `application/json`


#### Tags

* Products


<a name="apiproductsbyidget"></a>
### Gets an individual product
```
GET /api/Products/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the product|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* Products


<a name="apiproductsbyidput"></a>
### Not implemented yet
```
PUT /api/Products/{id}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**id**  <br>*required*|integer (int32)|
|**Body**|**value**  <br>*optional*|string|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Products


<a name="apiproductsbyiddelete"></a>
### Not implemented yet
```
DELETE /api/Products/{id}
```


#### Parameters

|Type|Name|Schema|
|---|---|---|
|**Path**|**id**  <br>*required*|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|


#### Tags

* Products




<a name="definitions"></a>
## Definitions

<a name="order"></a>
### Order

|Name|Schema|
|---|---|
|**endTime**  <br>*optional*|string (date-time)|
|**name**  <br>*required*|string|
|**orderId**  <br>*optional*|integer (int32)|
|**orderPositions**  <br>*optional*|< [OrderPosition](#orderposition) > array|
|**plannedStartTime**  <br>*optional*|string (date-time)|
|**priority**  <br>*optional*|enum (Low, Medium, High)|
|**startTime**  <br>*optional*|string (date-time)|
|**state**  <br>*optional*|enum (Created, Started, Completed)|


<a name="orderposition"></a>
### OrderPosition

|Name|Schema|
|---|---|
|**endTime**  <br>*optional*|string (date-time)|
|**order**  <br>*optional*|[Order](#order)|
|**orderPositionId**  <br>*optional*|integer (int32)|
|**plannedStartTime**  <br>*optional*|string (date-time)|
|**product**  <br>*optional*|[Product](#product)|
|**startTime**  <br>*optional*|string (date-time)|
|**state**  <br>*optional*|enum (Created, Started, Completed)|
|**workPlanNumber**  <br>*optional*|integer (int32)|


<a name="product"></a>
### Product

|Name|Schema|
|---|---|
|**description**  <br>*optional*|string|
|**orderPosition**  <br>*optional*|[OrderPosition](#orderposition)|
|**partNumber**  <br>*optional*|integer (int32)|
|**productId**  <br>*optional*|integer (int32)|
|**workPlanNumber**  <br>*optional*|integer (int32)|





