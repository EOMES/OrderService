﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Oxmes.Order.Enums;

namespace Oxmes.Order.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        public int PartNumber { get; set; }	//unique identifier of the product PNo
											//NOT the same as ProductID!!!

		public string Description { get; set; }

		public int WorkPlanNumber { get; set; } //unique identifier of the workplan WPNo

		//here the foreign key for the orderPosition needs to be removed
		[ForeignKey(nameof(OrderPosition))]
		public int? OrderPositionId { get; set; }
		public virtual OrderPosition OrderPosition { get; set; }

		
	}
}
