﻿using System;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Oxmes.Order.Enums;


namespace Oxmes.Order.Models
{
    public class Order
    {
		[Key]
		public int OrderId { get; set; }

		[Required]
		public string Name { get; set; }

		[Display(Name = "Start Time")]
		public DateTime StartTime { get; set; }

		[Display(Name = "End Time")]
		public DateTime EndTime { get; set; }

		[Required]
		[Display(Name = "Planned Start Time")]
		public DateTime PlannedStartTime { get; set; }

		public State State { get; set; }
		public Priority Priority { get; set; }

		public virtual ICollection<OrderPosition> OrderPositions { get; set; } = new List<OrderPosition>();
	}
}