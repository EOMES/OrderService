﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Oxmes.Order.Enums;

namespace Oxmes.Order.Models
{

	public class OrderPosition
    {
		[Key]
		public int OrderPositionId { get; set; }

		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public DateTime PlannedStartTime { get; set; }

		public int WorkPlanNumber { get; set; }
		public State State { get; set; }

		[ForeignKey(nameof(Order))]
		public int? OrderId { get; set; }
		public virtual Order Order { get; set; }
		
		//here the foreign key for the product needs to be added
		public virtual Product Product { get; set; } = new Product();
	}
}
