﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Oxmes.Core.EventBusClient;
using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Messages.Common;
using Oxmes.Core.ServiceTypes;
using Oxmes.Core.ServiceUtilities;
using Oxmes.Order.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Oxmes.Order.Services
{
	public class GatewayConnectorService : IGatewayConnectorService
	{
		private readonly ILogger<GatewayConnectorService> Logger;
		private readonly IServiceClient ServiceClient;
		private readonly IEventBusClient EventBusClient;
		private readonly EventBusMessageFactory EventBusMessageFactory;
		private readonly IConfiguration Configuration;

		private bool Connected { get; set; } = false;
		private bool Configured { get; set; } = false;
		private ConnectionLostPolicy ConnectionLostPolicy { get; set; }

		private Task WatchdogTask { get; set; }
		private CancellationTokenSource WatchdogTaskCancellationTokenSource { get; set; }

		public GatewayConnectorService(ILogger<GatewayConnectorService> logger, IServiceClient serviceClient, IEventBusClient eventBusClient, EventBusMessageFactory eventBusMessageFactory, IConfiguration configuration)
		{
			Logger = logger;
			ServiceClient = serviceClient;
			EventBusClient = eventBusClient;
			EventBusMessageFactory = eventBusMessageFactory;
			Configuration = configuration;
		}

		public void Configure(ConnectionLostPolicy connectionLostPolicy)
		{
			ServiceClient.ApiKey = "";
			ServiceClient.GatewayHost = Configuration["UserGateway:Host"];
			ServiceClient.GatewayPort = ushort.Parse(Configuration["UserGateway:Port"]);
			ServiceClient.UseHttps = false;
			ServiceClient.ServiceDisplayName = "Order Service";
			ServiceClient.ServiceName = "order-service";
			ServiceClient.ServiceType = (int)ServiceTypes.Order;

			EventBusClient.AuthorizationToken = "";
			EventBusClient.Host = Configuration["UserGateway:Host"];
			EventBusClient.Port = ushort.Parse(Configuration["UserGateway:Port"]);

			Configured = true;
			ConnectionLostPolicy = connectionLostPolicy;
		}

		public async Task ConnectAsync()
		{
			if (Configured)
			{
				Logger.LogInformation("Announcing service to User Gateway");
				await ServiceClient.ConnectAsync();

				// Configure EventBusMessageFactory
				EventBusMessageFactory.ServiceUuid = ServiceClient.ServiceUuid;

				Logger.LogInformation("Connecting to Event Bus");
				EventBusClient.ServiceUuid = ServiceClient.ServiceUuid;
				await EventBusClient.ConnectAsync();
				await EventBusClient.SubscribeAsync((int)CommonMessageTypes.ServiceShutdown);
				EventBusClient.OnEventMessageReceive += ServiceShutdownHandler;
				EventBusClient.OnEventMessageReceive += (s, a) => Console.WriteLine("Event");
				EventBusClient.AcceptEvents();

				Logger.LogInformation("Fully connected to gateway");
				Connected = true;
				if (ConnectionLostPolicy != ConnectionLostPolicy.Nothing) CreateWatchdogTask();
			}
		}

		private void CreateWatchdogTask()
		{
			var source = WatchdogTaskCancellationTokenSource = new CancellationTokenSource();
			var token = source.Token;
			WatchdogTask = Task.Run(async () =>
			{
				using (var httpClient = new HttpClient { BaseAddress = new Uri($"{(ServiceClient.UseHttps ? "https" : "http")}://{ServiceClient.GatewayHost}:{ServiceClient.GatewayPort}/api/Ping/") })
				{
					while (!token.IsCancellationRequested)
					{
						await Task.Delay(30000, token);
						token.ThrowIfCancellationRequested();
						try
						{
							var data = DateTimeOffset.Now.ToUnixTimeMilliseconds().ToString();
							var response = await httpClient.GetStringAsync(data);
							token.ThrowIfCancellationRequested();
							if (response != data)
							{
								Connected = false;
								Logger.LogWarning("Ping response different from expected value");
								switch (ConnectionLostPolicy)
								{
									case ConnectionLostPolicy.Reconnect:
										await ConnectAsync();
										return;
									case ConnectionLostPolicy.Shutdown:
										throw new NotImplementedException();
								}
							}
						}
						catch (HttpRequestException ex)
						{
							Connected = false;
							Logger.LogWarning("Gateway missing");
							switch (ConnectionLostPolicy)
							{
								case ConnectionLostPolicy.Reconnect:
									await ConnectAsync();
									return;
								case ConnectionLostPolicy.Shutdown:
									throw new NotImplementedException();
							}
						}
					}
				}
			}, token);
		}

		private async void ServiceShutdownHandler(object sender, EventBusMessageArgs args)
		{
			if (args.MessageType == typeof(ServiceShutdownMessage))
			{
				if (Guid.Parse((string)((ServiceShutdownMessage)args.Message).Payload) == ServiceClient.GatewayUuid)
				{
					Connected = false;
					switch (ConnectionLostPolicy)
					{
						case ConnectionLostPolicy.Nothing: break;
						case ConnectionLostPolicy.Reconnect:
							Logger.LogWarning("Lost connection to User Gateway. Will start attempting to reconnect");
							WatchdogTaskCancellationTokenSource.Cancel();
							await WatchdogTask.ContinueWith((prev) => { if (!Connected) ConnectAsync(); });
							break;
						case ConnectionLostPolicy.Shutdown:
							throw new NotImplementedException();
					}
				}
			}
		}
	}
}
