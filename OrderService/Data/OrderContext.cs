﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Oxmes.Order.Models;


namespace Oxmes.Order.Data
{
    public class OrderContext : DbContext
    {
        public OrderContext (DbContextOptions<OrderContext> options)
            : base(options)
        {
        }
		public DbSet<Oxmes.Order.Models.Order> Orders { get; set; }
		public DbSet<Oxmes.Order.Models.OrderPosition> OrderPositions { get; set; }
		public DbSet<Oxmes.Order.Models.Product> Products { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<Models.Order>()
				.HasMany(op => op.OrderPositions)
				.WithOne(o => o.Order)
				.IsRequired();

			modelBuilder.Entity<Models.OrderPosition>()
				.HasOne(p => p.Product)
				.WithOne(opo => opo.OrderPosition)
				.IsRequired();
		}
	}
}
