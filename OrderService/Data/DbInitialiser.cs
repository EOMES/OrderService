﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Oxmes.Order.Models;

namespace Oxmes.Order.Data
{
    public class DbInitialiser
    {
        public static async Task Initialise(OrderContext context)
        {
			var product = new Models.Product { PartNumber = 5001, Description = "phone", WorkPlanNumber = 42 };
			var orderposition1 = new Models.OrderPosition
			{
				StartTime = DateTime.Now,
				EndTime = DateTime.Now,
				PlannedStartTime = DateTime.Now,
				WorkPlanNumber = 44,
				State = Enums.State.Created };

			var orderposition2 = new Models.OrderPosition
			{
				StartTime = DateTime.Now,
				EndTime = DateTime.Now,
				PlannedStartTime = DateTime.Now,
				WorkPlanNumber = 45,
				State = Enums.State.Created
			};


			var order1 = new Models.Order { Name = "Dummy",
				StartTime = DateTime.Now, EndTime = DateTime.Now, PlannedStartTime = DateTime.Now,
				State = Enums.State.Completed, Priority = Enums.Priority.High};
			order1.OrderPositions.Add(orderposition1);
			System.Console.Write(order1);
			order1.OrderPositions.Add(orderposition2);

			await context.AddAsync(order1);
            await context.SaveChangesAsync();
        }
    }
}
