﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxmes.Order.Interfaces
{
    public interface IGatewayConnectorService
		{
			void Configure(ConnectionLostPolicy connectionLostPolicy);
			Task ConnectAsync();

		}

		public enum ConnectionLostPolicy
		{
			Nothing,
			Reconnect,
			Shutdown,
		}
	
}
