﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Order.Data;
using Oxmes.Order.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Oxmes.Order.Controllers
{
	[Produces("application/json")]
	[Route("api/OrderPositions")]
	public class OrderPositionsController : Controller
	{
		private readonly OrderContext _context;

		public OrderPositionsController(OrderContext context)
		{
			_context = context;
		}

		// GET: api/OrderPositions
		/// <summary>
		/// Gets all the Order positions
		/// </summary>
		/// <returns>A list of json objects containing
		/// all the order positions</returns>
		[HttpGet]
		public IEnumerable<Oxmes.Order.Models.OrderPosition> GetOrderPosition()
		{
			return _context.OrderPositions;
		}

		// GET: api/OrderPositions/5
		/// <summary>
		/// Gets an individual order position
		/// </summary>
		/// <param name="id">The ID of the order position</param>
		/// <returns>A json object containing the order position</returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOrderPosition([FromRoute] int? id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			var orderPosition = _context.OrderPositions
								.Where(u => u.OrderPositionId == id);

			if (orderPosition == null)
			{
				return NotFound();
			}

			return Ok(orderPosition);
		}

		// GET: api/OrderPositions/Order/5
		/// <summary>
		/// Gets all the order positions of an order
		/// </summary>
		/// <param name="id">The ID of the parent order</param>
		/// <returns>A list of json object containg order positions
		/// of an idividual order</returns>
		[HttpGet("Order/{id}")]
		public async Task<IActionResult> GetOrderPositionsByOrderId([FromRoute] int? id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}
			var orderPosition = _context.Orders
								.Where(u => u.OrderId == id)
								.SelectMany(u => u.OrderPositions)
								.OrderBy(p => p.OrderPositionId)
								.Take(10);

			if (orderPosition == null)
			{
				return NotFound();
			}

			return Ok(orderPosition);
		}

		// POST: api/OrderPositions
		/// <summary>
		/// Creates  an order position
		/// </summary>
		/// <param name="orderPosition"></param>
		/// <returns></returns>
		[HttpPost]
		public async Task<IActionResult> PostOrderPosition([FromBody] Oxmes.Order.Models.OrderPosition orderPosition)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.OrderPositions.Add(orderPosition);
			await _context.SaveChangesAsync();

			return CreatedAtAction("GetOrderPosition", new { id = orderPosition.OrderPositionId }, orderPosition);
		}

		// DELETE: api/OrderPositions/5
		/// <summary>
		/// Deletes an order position
		/// </summary>
		/// <param name="id">The ID of the order position to be deleted</param>
		/// <returns></returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteOrderPosition([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var orderPosition = await _context.OrderPositions.SingleOrDefaultAsync(m => m.OrderPositionId == id);
			if (orderPosition == null)
			{
				return NotFound();
			}

			_context.OrderPositions.Remove(orderPosition);
			await _context.SaveChangesAsync();

			return Ok(orderPosition);
		}

		
	}

}
