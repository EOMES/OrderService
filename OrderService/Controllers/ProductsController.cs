﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Order.Data;

namespace Order.Controllers
{
    [Produces("application/json")]
    [Route("api/Products")]
    public class ProductsController : Controller
    {
		private readonly OrderContext _context;

		public ProductsController(OrderContext context)
		{
			_context = context;
		}

		// GET: api/Products
		/// <summary>
		/// Gets all the products
		/// </summary>
		/// <returns>A list of json objects containing all the products</returns>
		[HttpGet]
		public IEnumerable<Oxmes.Order.Models.Product> GetProduct()
		{
			return _context.Products;
		}

		// GET: api/Prodcusts/5
		/// <summary>
		/// Gets an individual product
		/// </summary>
		/// <param name="id">The ID of the product</param>
		/// <returns>A json object containing the product</returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> GetProduct([FromRoute] int? id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var product = _context.Products
				.Where(u => u.ProductId == id);

			if (product == null)
			{
				return NotFound();
			}

			return Ok(product);
		}

		// POST: api/Products
		/// <summary>
		/// Not implemented yet
		/// </summary>
		/// <param name="value"></param>
		[HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Products/5
		/// <summary>
		/// Not implemented yet
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
		/// <summary>
		/// Not implemented yet
		/// </summary>
		/// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
