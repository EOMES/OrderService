﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Order.Data;
using Oxmes.Order.Models;

namespace Oxmes.Order.Controllers
{
    [Produces("application/json")]
    [Route("api/Orders")]
    public class OrdersController : Controller
    {
        private readonly OrderContext _context;

        public OrdersController(OrderContext context)
        {
            _context = context;
        }

        // GET: api/Orders
		/// <summary>
		/// Gets all the existing orders
		/// </summary>
		/// <returns>List of json objects containing orders and their
		/// components (order positions, respectively products)</returns>
        [HttpGet]
        public IEnumerable<Oxmes.Order.Models.Order> GetOrder()
        {
            return _context.Orders;
        }

		// GET: api/Orders/5
		/// <summary>
		/// Gets individual orders
		/// </summary>
		/// <param name="id">The ID of the order</param>
		/// <returns>A json object containing one order and its 
		/// components (order positions, respectively products)</returns>
		[HttpGet("{id}")]
        public async Task<IActionResult> GetOrder([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

			var order =  _context.Orders
				.Include(op => op.OrderPositions)
				.Where(i => i.OrderId == id);

			if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
		/// <summary>
		/// Updates the fields of an order
		/// </summary>
		/// <param name="id">The ID of the order to be updated</param>
		/// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrder([FromRoute] int id, [FromBody] Oxmes.Order.Models.Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.OrderId)
            {
                return BadRequest();
            }

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(order);
            //return NoContent();
        }

        // POST: api/Orders
		/// <summary>
		/// Adds a new order
		/// </summary>
		/// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostOrder([FromBody] Oxmes.Order.Models.Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOrder", new { id = order.OrderId }, order);
        }

        // DELETE: api/Orders/5
		/// <summary>
		/// Deletes an order
		/// </summary>
		/// <param name="id">The ID of the order to be deleted</param>
		/// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var order = await _context.Orders.SingleOrDefaultAsync(m => m.OrderId == id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();

            return Ok(order);
        }

        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.OrderId == id);
        }
    }
}