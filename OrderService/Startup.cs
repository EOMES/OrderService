﻿using System;
using System.Collections.Generic;
using Oxmes.Core.EventBusClient;
using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Messages.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;

using Pomelo.EntityFrameworkCore.MySql;


using Oxmes.Core.ServiceTypes;
using Oxmes.Core.ServiceUtilities;
using Oxmes.Order.Data;
using Oxmes.Order.Interfaces;
using Oxmes.Order.Services;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;

namespace Oxmes.Order
{
    public class Startup
    {
        private readonly ILogger Logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger) //dependecy injection
        {
            Configuration = configuration;
            Logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddMvc().AddJsonOptions(options =>
			{
				options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
			});

			services.AddSingleton<IEventBusClient, EventBusClient>();
            services.AddSingleton<IServiceClient, ServiceClient>();
            services.AddSingleton<EventBusMessageFactory>();
			services.AddSingleton<IGatewayConnectorService, GatewayConnectorService>();

			services.AddDbContext<OrderContext>(options =>
                    options.UseMySql(Configuration.GetConnectionString("OrderContext")));

			//Swashbuckle.Bootstrapper.Init(config);

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "Order Service API", Version = "v1" });
				var basePath = PlatformServices.Default.Application.ApplicationBasePath;
				var xmlPath = Path.Combine(basePath, "OrderSwagger.xml");
				c.IncludeXmlComments(xmlPath);
				c.DescribeAllEnumsAsStrings();
			});



		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            lifetime.ApplicationStarted.Register(ApplicationStarted, app.ApplicationServices);
            lifetime.ApplicationStopping.Register(ApplicationStopping, app.ApplicationServices);

            app.UseMvc();

			// Serve Swagger Json 
			app.UseSwagger();

			// Enable Swagger UI 
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Order Service API v1");
			});

			
		}

        private async void ApplicationStarted(object applicationServices)
        {
			if (applicationServices is IServiceProvider services)
			{
				var gatewayConnectorService = services.GetRequiredService<IGatewayConnectorService>();
				gatewayConnectorService.Configure(ConnectionLostPolicy.Reconnect);
				await gatewayConnectorService.ConnectAsync();

				Logger.LogInformation("Initialisation finished");
			}
		}

        private void ApplicationStopping(object applicationServices)
        {
            if (applicationServices is IServiceProvider services)
            {
                Logger.LogInformation("Disconnecting from Event Bus");
                var messageFactory = services.GetRequiredService<EventBusMessageFactory>();
                var eventBusClient = services.GetRequiredService<IEventBusClient>();
                eventBusClient.SendEventAsync(messageFactory.CreateControlMessage<ServiceShutdownMessage>(null)).Wait();
                eventBusClient.CloseAsync().Wait();
            }
        }
    }
}
