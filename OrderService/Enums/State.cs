﻿

namespace Oxmes.Order.Enums
{ 
	public enum State
	{
		Created,
		Started,
		Completed
	}
}